message(STATUS "Using helper repo at ${_collection} for CPM dependencies.")

foreach(dep findmkl_cmake CPMLicenses.cmake)
    set(CPM_${dep}_SOURCE ${_collection}/bilke/${dep})
endforeach()

foreach(
    dep
    autocheck
    cvode
    iphreeqc
    json-cmake
    rapidxml
    SwmmInterface
    tclap
    tetgen
    vtkdiff
)
    set(CPM_${dep}_SOURCE ${_collection}/ufz/${dep})
endforeach()

foreach(dep xmlpatch exprtk xdmf)
    set(CPM_${dep}_SOURCE ${_collection}/ogs/libs/${dep})
endforeach()

set(CPM_GroupSourcesByFolder.cmake_SOURCE
    ${_collection}/TheLartians/GroupSourcesByFolder.cmake
)
